from Controllers.MercuryInstanceManager import MercuryInstanceManager

class MercuryGUIManager():
    def __init__(self):
        self.InstanceManager = MercuryInstanceManager()

    def generateBaseNoise(self):
        self.InstanceManager.generateBaseNoise()

    def getDisplayBaseNoiseImage(self):
        return self.InstanceManager.getDisplayBaseNoiseImage()

    def setProperty(self, name, val):
        self.InstanceManager.setProperty(name, val)

    def getProperty(self, name):
        return self.InstanceManager.getProperty(name)