from lib.Layers.NoiseGenerators.generators import *
from lib.Graphics.MapArtist import *
import lib.Graphics.graphics as G
from Model.MercuryDataStore import MercuryDataStore, Props

class MercuryInstanceManager():
    def __init__(self):
        self.store = MercuryDataStore()
        pass

    def generateBaseNoise(self):
        self.store.baseNoise = Generator.generateNoiseLayer(self.store.properties.size[0], self.store.properties.size[1])

    def getBaseNoise(self):
        return self.store.baseNoise

    def getDisplayBaseNoiseImage(self):
        array = self.getBaseNoise()
        im = G.arrayToImage(array, 'F').convert(mode='L')
        return im

    def GetNoiseImage(self):
        arr = Generator.generateLandSeaArray(xSize=100, ySize=100)
        im = G.arrayToImage(arr, 'F')
        return im
    
    def setProperty(self, name, value):
        setattr(self.store.properties, name, value)
    
    def getProperty(self, name):
        return getattr(self.store.properties, name)
