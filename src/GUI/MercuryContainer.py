import sys
from PyQt5.Qt import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from GUI.ThresholderPage import MainPanel as ThresholderMainPanel
from Controllers.MercuryGuiManager import MercuryGUIManager

class MercuryContainer(QWidget):
    def __init__(self):
        super(MercuryContainer, self).__init__()

        self.guiManager = MercuryGUIManager()


        self.pages = []

        # populate the pages of the app
        self.pages.append(ThresholderMainPanel(self.guiManager))


        # populate the stack with the pages
        self.Stack = QStackedWidget(self)
        for i in range(len(self.pages)):
            self.Stack.addWidget(self.pages[i])

        # display
        hbox = QHBoxLayout(self)
        hbox.addWidget(self.Stack)
        self.setLayout(hbox)
        self.setGeometry(100, 100, 800, 600)
        self.setWindowTitle('MercuryGen')

    def run(self):
        self.show()