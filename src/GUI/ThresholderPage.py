import sys
from PyQt5.Qt import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
from PIL.ImageQt import ImageQt
from Controllers.MercuryGuiManager import MercuryGUIManager

class MainPanel(QWidget):
    def __init__(self, guiManager: MercuryGUIManager):
        QWidget.__init__(self)  
        self.guiManager = guiManager

        self.layout = QGridLayout()

        self.image = QLabel(self)
        self.image.setScaledContents(True)
        self.image.setFixedSize(500, 500)
        self.image.setPixmap(QPixmap(""))
        self.layout.addWidget(self.image, 1, 1)
        
        self.threshold_sliders = SlidersPanel(self.guiManager)
        self.threshold_sliders.generation_finished.connect(self.on_generation_finished)
        self.layout.addWidget(self.threshold_sliders, 1, 2)

        # Set the VBox layout as the window's main layout
        self.setLayout(self.layout)

    def on_generation_finished(self):
        im = self.guiManager.getDisplayBaseNoiseImage()
        self.image.setPixmap(QPixmap.fromImage(ImageQt(im)))

class SlidersPanel(QWidget):
    generation_finished = pyqtSignal()

    def __init__(self, guiManager: MercuryGUIManager):
        QWidget.__init__(self)
        self.guiManager = guiManager
        self.initGui()

    def generateBaseNoise(self):
        self.guiManager.setProperty('size', (int(self.xSizeInput.text()), int(self.ySizeInput.text())))
        self.guiManager.generateBaseNoise()
        self.generation_finished.emit()

    def initGui(self):
        self.layout = QFormLayout(self)
        self.layout.setRowWrapPolicy(QFormLayout.WrapLongRows)
        self.layout.setFieldGrowthPolicy(QFormLayout.AllNonFixedFieldsGrow)
        self.setFixedWidth(300)

        self.xSizeLabel = QLabel(self)
        self.xSizeLabel.setText("Width")
        self.xSizeInput = QLineEdit(self)
        self.xSizeInput.setValidator(QIntValidator())

        self.ySizeLabel = QLabel(self)
        self.ySizeLabel.setText("Height")
        self.ySizeInput = QLineEdit(self)
        self.ySizeInput.setValidator(QIntValidator())

        self.thresholdLabel = QLabel(self)
        self.thresholdLabel.setText("Threshold")
        self.thresholdSlider = QSlider(Qt.Horizontal, self)


        self.checkBox = QCheckBox('Enable Threshold', self)
        # self.checkBox.stateChanged.connect()

        self.generateButton = QPushButton("Generate Map", self)
        self.generateButton.clicked.connect(self.generateBaseNoise)

        self.layout.addRow(self.xSizeLabel, self.xSizeInput)
        self.layout.addRow(self.ySizeLabel, self.ySizeInput)
        self.layout.addRow(self.thresholdLabel, self.thresholdSlider)
        self.layout.addRow(self.checkBox)

        self.layout.addRow(self.generateButton)