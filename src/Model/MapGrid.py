from dataclasses import dataclass
from typing import Set, Dict, Tuple

Point = Tuple[float, float]
PointArray = Set[Point]
CellArray = Dict[Point, 'DelCellData']

@dataclass
class DelCellData:
    neighbors: PointArray
    vertices: PointArray
    coordinates: Point
    isEdge: bool

@dataclass
class VorCellData:
    neighbors: PointArray
    vertices: PointArray
    coordinates: Point
    isEdge: bool

@dataclass
class MapData:
    delCentroids: PointArray = None
    vorCentroids: PointArray = None