from Model.MapGrid import MapData
from dataclasses import dataclass
from typing import Set, Dict, Tuple
import numpy as np

@dataclass
class Props:
    size: Tuple[int, int] = (0, 0)

@dataclass
class MercuryDataStore:
    mapData: MapData = MapData()
    baseNoise: np.ndarray = None
    properties: Props = Props()