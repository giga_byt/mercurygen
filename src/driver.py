import lib.Grid.grid as grid
import lib.Graphics.graphics as G
import lib.Layers.NoiseGenerators.generators as gen
from PIL import Image, ImageOps
from lib.Graphics.MapArtist import MapArtist
import lib.constant as constant
import numpy as np
from scipy.ndimage.filters import gaussian_filter
from GUI.MercuryContainer import MercuryContainer
import sys
from PyQt5.Qt import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *

def main():
    # Creating underlying grid
    mapgrid = grid.MapGrid(500, 500, 500)
    mapgrid.generate_grid(10)

    coastlines = gen.Generator.generateLandSeaArray(500, 500, 128, 3, 1, 34)
    artist = MapArtist(mapgrid)
    artist.initialize_image()
    for point in mapgrid.del_cents:
        if(coastlines[int(point[1])][int(point[0])] == 0):
            artist.fill_cell(point, 'blue')
        else:
            artist.fill_cell(point, 'green')
        artist.draw_point(point, 'black')
    artist.draw_vor_grid()
    artist.display_image()
    G.arrayToImage(coastlines, 'L').show()

def generatorTest():
    testSeed = seed = np.random.randint(1, 100)
    print(testSeed)
    coastlines = gen.Generator.generateLandSeaArray(xSize=250, ySize=250, threshold=128, detail=8, scale=1, seed=76)
    G.arrayToImage(coastlines, 'L').show()


def noisifyTest():
    im = Image.open("noisifyTest.png").convert('L')
    im = ImageOps.invert(im)
    array = G.imageToArray(im, "L")
    res = gaussian_filter(array, 30)
    G.arrayToImage(res,"L").show()
    # comp = gen.Generator.generateLandSeaArray(xSize=250, ySize=250, threshold=128, detail=8, scale=1, seed=18)
    new_array = gen.Generator.noisifyLandSeaArray(array, spread=10, destructiveness=0.3, seed=18)
    G.arrayToImage(new_array, 'L').show()
    # G.arrayToImage(comp, 'L').show()

if __name__ == '__main__':
    qt_app = QApplication(sys.argv)
    qt_app.setStyle(QStyleFactory.create("Plastique"))
    app = MercuryContainer()
    app.run()
    qt_app.exec_()