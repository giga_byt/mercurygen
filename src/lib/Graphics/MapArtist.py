import numpy as np
import random as r
import lib.constant as constant
from PIL import Image, ImageDraw
from PIL.ImageDraw import Draw
import lib.Grid.grid as grid

class MapArtist():
    def __init__(self, mapgrid):
        self.mapgrid = mapgrid
        self.im = None

    def reset_image(self, mapgrid):
        self.mapgrid = mapgrid
        self.im = None

    def display_image(self):
        if(self.im):
            self.im.show()
    
    def initialize_image(self, bcolor=constant.COLOR_VOR_BACKGROUND):
        height = self.mapgrid.get_height()
        width = self.mapgrid.get_width()
        self.im = Image.new(mode='RGBA', size=(height, width), color=bcolor)


    def draw_vor_grid(self):
        # draw every single edge
        pen = ImageDraw.Draw(self.im)
        for region in self.mapgrid.array_vor_regions():
            startpoints = region
            endpoints = np.roll(startpoints, 1)
            for i in range(len(startpoints)):
                line = (self.mapgrid.get_vor_vertex([startpoints[i]]), self.mapgrid.get_vor_vertex([endpoints[i]]))
                pen.line(line, fill=constant.COLOR_VOR_OUTLINE)

    def draw_del_grid(self):
        # draw every single edge
        pen = ImageDraw.Draw(self.im)
        for region in self.mapgrid.array_del_regions():
            startpoints = region
            endpoints = np.roll(startpoints, 1)
            for i in range(len(startpoints)):
                line = (self.mapgrid.get_del_vertex([startpoints[i]]), self.mapgrid.get_del_vertex([endpoints[i]]))
                pen.line(line, fill=constant.COLOR_DEL_OUTLINE)
    
    def fill_cell(self, point, color):
        pointData = self.mapgrid.mapPoints.get(point)
        if(pointData):
            vertices = list(pointData.vertices)
            pen = ImageDraw.Draw(self.im)
            pen.polygon(vertices, fill=color)

    def draw_point(self, point, color):
        pen = ImageDraw.Draw(self.im)
        pen.point(point, fill=color)