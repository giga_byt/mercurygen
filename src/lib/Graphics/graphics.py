import numpy as np
import random as r
import lib.constant as constant
from PIL import Image, ImageDraw
from PIL.ImageDraw import Draw
import lib.Grid.grid as grid

def arrayToImage(data, mode):
    if(mode == "F"):
        im = Image.fromarray(data)
        return im
    if(mode == "RGB"):
        return
    if(mode == "hsv"):
        return
    return

def imageToArray(im, mode):
    if(mode == "L"):
        im.convert("L")
        return np.array(im)