import scipy.spatial as sci
import numpy as np
import random as r
import lib.constant as constant
from PIL import Image, ImageDraw
import lib.Math.lib_geometry as Geo
from collections import namedtuple
import lib.Grid.points as mp
import lib.Grid.grid_utils as GU
from lib.bridson.bridson import poisson_disc_samples

class MapGrid:
    'Data Representation of Map Grid'
    def __init__(self, max_x, max_y, num):
        # Meta Data
        self.num_pts = 0
        self.size = namedtuple('Size', ['x', 'y'])

        # Directly Modifiable/Original Data
        self.vor_cent_pts = []

        # Useful Data
        self.mapPoints = {}
        self.del_cents = set()
        self.vor_cents = set()

        # Derived Data
        self.delaunay = None
        self.voronoi = None

        self.size.x = max_x
        self.size.y = max_y
        self.num_pts = num

    # ============================================== #
    #          Generation/Data Manipulation Functions
    # ============================================== #
    def generate_grid(self, relax = 2):
        self.vor_cent_pts = poisson_disc_samples(width = self.size.x, height=self.size.y, r=10)
        # self.vor_cent_pts = np.array([[np.random.randint(1, self.size.x), np.random.randint(1, self.size.y)] for _ in range(self.num_pts)])
        self.vor_cent_pts = GU.relax_points(self.vor_cent_pts, relax, self.size)
        self.delaunay = GU.generate_delaunay(self.vor_cent_pts, self.size)
        self.voronoi = GU.generate_voronoi(self.vor_cent_pts, self.size)
        self.voronoi = GU.clean_voronoi_irregularities(self.voronoi, self.delaunay)
        self._populate_point_data()
        iterations = 0
        while len(self.del_cents) != len(self.delaunay.filtered_simplices):
            iterations += 1
            print(iterations)
            if(iterations > 50):
                print("Error: Likely Flawed Map, please regenerate")
                return
            doubles = self.find_doubled_simplex_centers()
            self.vor_cent_pts = np.concatenate((self.vor_cent_pts, doubles), axis=0)
            self.vor_cent_pts = GU.relax_points(self.vor_cent_pts, 1, self.size)
            self.delaunay = GU.generate_delaunay(self.vor_cent_pts, self.size)
            self.voronoi = GU.generate_voronoi(self.vor_cent_pts, self.size)
            self.voronoi = GU.clean_voronoi_irregularities(self.voronoi, self.delaunay)
            self._populate_point_data()
        print("Grid Generation Complete")
        
    def generate_kd(self):
        self.del_kd = sci.KDTree(self.vor_cent_pts)

    def find_doubled_simplex_centers(self):
        seen = set()
        doubles = []
        for simplex in self.delaunay.filtered_simplices:
            common = GU.get_simplex_center(self.voronoi, self.delaunay, simplex)
            center = tuple(self.voronoi.vertices[common])
            if center in seen:
                doubles.append(center)
            else:
                seen.add(center)
        return doubles

    def _populate_point_data(self):
        self.del_cents = set()
        self.vor_cents = set()
        # Initialize every map point with its region indicators
        for simplex in self.delaunay.filtered_simplices:
            common = GU.get_simplex_center(self.voronoi, self.delaunay, simplex)
            center = tuple(self.voronoi.vertices[common])
            vertices = self.delaunay.points[simplex]
            vertices = tuple(map(tuple, vertices))
            self.del_cents.add(center)
            self.mapPoints[center] = mp.TriangleCell(center)
            self.mapPoints[center].vertices = set(vertices)

        for vor_centroid in self.vor_cent_pts:
            reg = GU.get_region_from_vor_centroid(self.voronoi, vor_centroid)
            vertices = self.voronoi.vertices[reg]
            vor_centroid = tuple(vor_centroid)
            vertices = tuple(map(tuple, vertices))
            self.vor_cents.add(vor_centroid)
            self.mapPoints[vor_centroid] = mp.VoronoiCell(vor_centroid)
            self.mapPoints[vor_centroid].vertices = set(vertices)
        
        # Now, start populating neighbors
        for vor_centroid in self.vor_cent_pts:
            reg = GU.get_region_from_vor_centroid(self.voronoi, vor_centroid)
            vertices = self.voronoi.vertices[reg]
            vertices = tuple(map(tuple, vertices))
            vertices2 = np.roll(vertices, 1, axis=0)
            vertices2 = tuple(map(tuple, vertices2))
            for i, j in zip(vertices, vertices2):
                p1 = self.mapPoints.get(i)
                p2 = self.mapPoints.get(j)
                if p1 and p2:
                    p1.neighbors.add(i)
                    p2.neighbors.add(j)
                    
        for simplex in self.delaunay.filtered_simplices:
            vertices = self.delaunay.points[simplex]
            vertices = tuple(map(tuple, vertices))
            vertices2 = np.roll(vertices, 1, axis=0)
            vertices2 = tuple(map(tuple, vertices2))
            for i, j in zip(vertices, vertices2):
                p1 = self.mapPoints.get(i)
                p2 = self.mapPoints.get(j)
                if p1 and p2:
                    p1.neighbors.add(i)
                    p2.neighbors.add(j)


    # ============================================== #
    #          Accessor Functions
    # ============================================== #
    def array_del_vertices(self):
        return self.delaunay.points

    def array_vor_vertices(self):
        return self.voronoi.vertices

    def array_vor_regions(self):
        return self.voronoi.filtered_regions

    def array_del_regions(self):
        return self.delaunay.filtered_simplices

    def get_del_vertex(self, idx):
        return (self.delaunay.points[idx][0][0],self.delaunay.points[idx][0][1])

    def get_vor_vertex(self, idx):
        return (self.voronoi.vertices[idx][0][0], self.voronoi.vertices[idx][0][1])

    def get_height(self):
        return self.size.y

    def get_width(self):
        return self.size.x


    # ============================================== #
    #
    # ============================================== #
