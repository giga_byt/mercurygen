import scipy.spatial as sci
import numpy as np
import lib.Math.lib_geometry as Geo

"""
Takes in unrelaxed array of points, relaxes and cleans them.
"""
def relax_points(pts, relax, size):
    tmpPts = pts
    for i in range(relax):
        tmpPts = lloyd_relax(tmpPts, size)
    tmpPts = clean_points(tmpPts)
    return tmpPts


"""
Given a list of points and the size of the field, generates a voronoi
graph bounded by the size.

Access the centroids with 
    @return.filtered_points
and its regions with
    @return.filtered_regions
"""
def generate_voronoi(pts, size):
    num_pts = len(pts)
    reflected_points = np.zeros(shape=(num_pts * 5, 2))
    i = 0
    # apply reflection
    for p in pts:
        reflected_points[i] = p
        # reflect left
        reflected_points[i + 1] = [p[0] * -1, p[1]]
        # reflect down
        reflected_points[i + 2] = [p[0], p[1] * -1]
        # reflect up
        reflected_points[i + 3] = [p[0], size.y * 2 - p[1]]
        # reflect right
        reflected_points[i + 4] = [size.x * 2 - p[0], p[1]]
        i += 5
    # Generate Voronoi from those points and bound by map resolution
    voronoi = sci.Voronoi(reflected_points)
    bounded_regions = []
    internal_vertices = []
    for region in voronoi.regions:
        flag = True
        for index in region:
            if index == -1:
                flag = False
                break
            else:
                x = voronoi.vertices[index, 0]
                y = voronoi.vertices[index, 1]
                if not (x >= -1 and x <= size.x + 1 and
                        y >= -1 and y <= size.y + 1):
                    flag = False
                    break
        if region != [] and flag:
            bounded_regions.append(region)
    voronoi.filtered_regions = bounded_regions
    voronoi.filtered_points = pts
    return voronoi


"""
Given a list of points and the size of the field, generates a Delaunay
graph bounded by the size.

Access its regions with
    @return.filtered_simplices
"""
def generate_delaunay(pts, size):
    num_pts = len(pts)
    reflected_points = np.zeros(shape=(num_pts * 5, 2))
    i = 0
    for p in pts:
        reflected_points[i] = p
        # reflect left
        reflected_points[i + 1] = [p[0] * -1, p[1]]
        # reflect down
        reflected_points[i + 2] = [p[0], p[1] * -1]
        # reflect up
        reflected_points[i + 3] = [p[0], size.y * 2 - p[1]]
        # reflect right
        reflected_points[i + 4] = [size.x * 2 - p[0], p[1]]
        i += 5
    delaunay = sci.Delaunay(reflected_points)
    insimplices = []
    for simplex in delaunay.simplices:
        flag = True
        for index in simplex:
            x = delaunay.points[index, 0]
            y = delaunay.points[index, 1]
            if not (x >= 0 and x <= size.x and y >= 0 and y <= size.y):
                flag = False
                break
        if simplex != [] and flag:
            insimplices.append(simplex)
    delaunay.filtered_simplices = insimplices
    return delaunay


"""
Conducts Lloyd relaxation on a set of points constrained by a 
field of a specific size.
In addition, this has a special case built in that ensures no vertex
appears in more than three voronoi cells: this is crucial for our
map assumptions.
    @argument pts array of unrelaxed points
    @argument size dimensions of field
    @return array of relaxed points
"""
def lloyd_relax(pts, size):
    voronoi = generate_voronoi(pts, size)
    seen = {}
    centroids = []
    for region in voronoi.filtered_regions:
        vertices = voronoi.vertices[region]
        centroids.append(Geo.centroid(vertices))
    centroids = np.array(centroids)
    return centroids
        
"""
Using a Delaunay Graph, adjusts Voronoi Vertices such that they lie within their corresponding
Delaunay triangle. The vertices are then cleaned and rounded.
"""
def clean_voronoi_irregularities(voronoi, delaunay):
    for simplex in delaunay.filtered_simplices:
        common = get_simplex_center(voronoi, delaunay, simplex)
        points = delaunay.points[simplex]
        if(Geo.inTri(points, voronoi.vertices[common])):
            continue
        else:
            cent = Geo.centroid(points)
            voronoi.vertices[common][0] = cent[0]
            voronoi.vertices[common][1] = cent[1]
    voronoi.vertices = clean_points(voronoi.vertices)
    return voronoi

"""
Given a simplex from a delaunay graph, and the voronoi graph,
returns the index (in voronoi.vertices) of the voronoi vertex 
that corresponds to that simplex.
"""
def get_simplex_center(voronoi, delaunay, simplex):
    point_a = delaunay.points[simplex[0]]
    point_b = delaunay.points[simplex[1]]
    point_c = delaunay.points[simplex[2]]

    index_a = np.where((voronoi.points[:,0] == point_a[0]) & (voronoi.points[:,1] == point_a[1]))
    index_b = np.where((voronoi.points[:,0] == point_b[0]) & (voronoi.points[:,1] == point_b[1]))
    index_c = np.where((voronoi.points[:,0] == point_c[0]) & (voronoi.points[:,1] == point_c[1]))

    reg_idx_a = voronoi.point_region[index_a[0]]
    reg_idx_b = voronoi.point_region[index_b[0]]
    reg_idx_c = voronoi.point_region[index_c[0]]

    reg_a = voronoi.regions[reg_idx_a[0]]
    reg_b = voronoi.regions[reg_idx_b[0]]
    reg_c = voronoi.regions[reg_idx_c[0]]
    common = np.intersect1d(np.intersect1d(reg_a, reg_b), reg_c)[0]
    return common


"""
Given a centroid from a voronoi graph, and the voronoi graph,
returns the index (in voronoi.vertices) of the voronoi region 
that corresponds to that centroid.
"""
def get_region_from_vor_centroid(voronoi, point):
    index = np.where((voronoi.points[:,0] == point[0]) & (voronoi.points[:,1] == point[1]))
    reg_idx = voronoi.point_region[index[0]]
    reg = voronoi.regions[reg_idx[0]]
    vertices = voronoi.vertices[reg]
    return reg

"""
Given a voronoi vertex, determines whether or not it's one of the edges of the map
"""
def isEdgePoint(point, size):
    if(point[0] == size.x or point[0] == 0):
        return true
    if(point[1] == size.y or point[1] == 0):
        return true
    return false


"""
Rounds points to the nearest whole number
"""
def clean_points(pts):
    tmpPts = []
    for point in pts:
        tmpPoint = point
        tmpPoint[0] = int(round(point[0]))
        tmpPoint[1] = int(round(point[1]))
        tmpPts.append(tmpPoint)
    return np.array(tmpPts)
