class TriangleCell():
    '''
    Data Representation of a single Delaunay Cell
    Corresponds to one Delunay Centroid/Voronoi Vertex
    '''
    def __init__(self, loc):
        self.neighbors = set()
        self.vertices = set()
        self.coordinates = loc
        self.touchesEdge = False
        self.isSea = False





class VoronoiCell():
    '''
    Data Representation of a single Voronoi Cell
    Corresponds to one Voronoi Centroid/Delaunay Vertex
    '''
    def __init__(self, loc):
        self.neighbors = set()
        self.vertices = set()
        self.coordinates = loc
        self.touchesEdge = False