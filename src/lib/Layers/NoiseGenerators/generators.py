import numpy as np
from PIL import Image
import lib.Layers.NoiseGenerators.noiseGen as noisegen
from scipy.ndimage.filters import gaussian_filter

class Generator:
    @staticmethod
    def generateNoiseLayer(xSize: int = 500, ySize:int = 500, scale: int = 1, seed: int = 0):
        ng = noisegen.NoiseGenerator(seed)
        size_compensated_scale = np.sqrt((500 * 500)/(xSize * ySize)) * scale
        arr = np.zeros(shape=(xSize, ySize))
        for x in range(xSize):
            for y in range(ySize):
                nx = x * size_compensated_scale
                ny = y * size_compensated_scale
                arr[x][y] = ng.genNoisePoint(nx, ny, 0, 255)
        return arr
    
    """
    Creates an array "image" of randomly generated landmass/seamass.
    Threshold indicates the proportion of sea to land 
    - a lower threshold results in more land and vice versa.
    For best results, I recommend thresholds between 100 and 156
    In addition, all parameters are set more or less optimally - scale in particular, 
    at higher numbers, fails to be meaningful. If generating whole continents,
    I recommend supplying an image.
    """
    @staticmethod
    def generateLandSeaArray(xSize=500, ySize=500, scale=1, seed=0):
        ng = noisegen.NoiseGenerator(seed)
        size_compensated_scale = np.sqrt((500 * 500)/(xSize * ySize)) * scale
        arr = np.zeros(shape=(xSize, ySize))
        for x in range(xSize):
            for y in range(ySize):
                nx = x * size_compensated_scale
                ny = y * size_compensated_scale
                arr[x][y] = ng.genNoisePoint(nx, ny, 0, 255)
        return arr
    
    @staticmethod
    def generatePlainSimplex():
        arr = np.zeros(shape=(500, 500))
        ng = noisegen.NoiseGenerator()
        for x in range(500):
            for y in range(500):
                arr[x][y] = ng.genNoisePoint(8, x, y, 0.5, 0.003, 0, 255)
        return arr
    
    @staticmethod
    def noisifyLandSeaArray(array, spread=30, destructiveness=0.5, threshold=128, detail=8, scale=1, seed=0):
        ng = noisegen.NoiseGenerator(seed)
        size = array.shape
        xSize = size[0]
        ySize = size[1]
        size_compensated_scale = np.sqrt((500 * 500)/(xSize * ySize)) * scale

        res = gaussian_filter(array, spread)
        res = res / 255
        res = res - destructiveness
        for x in range(xSize):
            for y in range(ySize):
                nx = x * size_compensated_scale
                ny = y * size_compensated_scale
                if (ng.noisifyPoint(res[x][y], destructiveness, detail-1, nx, ny, 0.5, 0.003, 0, 255) < threshold):
                    res[x][y] = 0
                else:
                    res[x][y] = 255
        return res
    