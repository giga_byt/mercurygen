from opensimplex import OpenSimplex
import numpy as np



class NoiseGenerator():
    def __init__(self, seed=0, octaves=8, persistence=0.5, scale=0.003):
        self.simplexer = OpenSimplex(seed)
        self.seed = seed
        self.octaves = octaves
        self.persistence = persistence
        self.scale = scale

    def randomize():
        self.seed = np.random.randint(1, 100)
        self.simplexer = OpenSimplex(self.seed)

    def genNoisePoint(self, x, y, min, max):
        maxAmp = 0
        amp = 1
        freq = self.scale
        noise = 0

        for i in range(self.octaves):
            noise += self.simplexer.noise2d(x * freq, y * freq) * amp
            maxAmp += amp
            amp *= self.persistence
            freq *= 2
        
        noise /= maxAmp
        noise = noise * (max - min) / 2 + (max + min) / 2
        return noise
    
    def noisifyPoint(self, start, preamp, octaves, x, y, persistence, scale, min, max):
        maxAmp = 1
        amp = 0.5
        freq = scale * 2
        noise = start

        for i in range(octaves):
            noise += self.simplexer.noise2d(x * freq, y * freq) * amp
            maxAmp += amp
            amp *= persistence
            freq *= 2
        
        noise /= maxAmp
        noise = noise * (max - min) / 2 + (max + min) / 2
        return noise