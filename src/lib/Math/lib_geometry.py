import numpy as np
"""
Input:
    An array containing vertices of a triangle, and a testing point
Output:
    True or False, whether the triangle contains the point (unpredictable behavior w/r/t edges)
"""
def inTri(tri, p):
    a = tri[0]
    b = tri[1]
    c = tri[2]
    sign_A = np.cross(vec3(a, p), vec3(a, b))[2] > 0
    sign_B = np.cross(vec3(b, p), vec3(b, c))[2] > 0
    sign_C = np.cross(vec3(c, p), vec3(c, a))[2] > 0
    if(sign_A == sign_B and sign_B == sign_C):
        return True
    else:
        return False
"""
Input:
    a = (x, y)
    b = (x, y)
Output:
    vector from a to b in 3d
"""
def vec3(a, b):
    return (tuple(np.subtract(b, a)) + (0,0))[:3]

"""
Input: list of points in a polygon
Output: Coordinate of the geometric centroid
"""
def centroid(lstP):
    xcoords = lstP[:, 0]
    ycoords = lstP[:, 1]
    Cx = np.abs(np.dot((xcoords + np.roll(xcoords, -1)),
            np.multiply(xcoords, np.roll(ycoords, -1)) - np.multiply(np.roll(xcoords, -1), ycoords)))
    Cy = np.abs(np.dot((ycoords + np.roll(ycoords, -1)),
            np.multiply(xcoords, np.roll(ycoords, -1)) - np.multiply(np.roll(xcoords, -1), ycoords)))
    ar = PolyArea(xcoords, ycoords)
    centr = ((1.0/(6.0*ar)*Cx),(1.0/(6.0*ar))*Cy)
    return centr

"""
Input: Given that polygons are represented by a list of vertices:
    x = all x-values in clockwise/counterclockwise order
    y = all y-values in cw/ccw order
Output:
    Area of corresponding polygon
"""

def PolyArea(x,y):
    return 0.5 * np.abs(np.dot(x,np.roll(y,1))-np.dot(y,np.roll(x,1)))
