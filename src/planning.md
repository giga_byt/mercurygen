TODO: Figure out how to, given a B/W coastline image, noisify it before converting it to a grid representation.

IDEA: HSV MAP

// @TODO: GENERATE RANDOM COASTLINE

// ======= At this point, we have a coastline, annotated land/sea polygons, and a grid underlying ====//

// generate perlin/simplex noise for heightmap:
// - lowerbound land polys at the coastline
// - lowerbound land polys receding from coastline (we don't want the coastline to be the edge of a bowl keeping the water out)
// - dual-pass; one enormous simplex for highlands and lowlands
// - @TODO: how do we generate interesting features?

// upper-bounding the water-polys
// - possibly just generate simplex from 0 to water_height or even lower, receding based on distance from coast
// - dual simplex? one for massive height sweeps, for seafloor and shallows differentiation, then another for island generation on shallows
// - "islands" option - if no, see above: if yes, scale simplex to just-above water_height so that islands may appear











// rivers: areas in which it rains, each corner point in the "cloud"
