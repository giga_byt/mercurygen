from generators import Generator as Gen
import graphics as G

arr = Gen.generateLandSeaArray(500, 500, 128)
G.arrayToImage(arr, "L").show()